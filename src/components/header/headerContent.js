import { Component } from "react";
import ImageHeader from "./image-header/imageHeader";
import TextHeader from "./text-header/textHeader";

class HeaderContent extends Component {
    render() {
        return (
            <>
                <TextHeader/>
                <ImageHeader/>
            </>
        )
    }
}

export default HeaderContent;