import { Component } from "react";
import InputBody from "./input-body/inputBody";
import OutputBody from "./output-body/outputBody";


class BodyContent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            inputMessage:"",
            outputMessage:[],
            likeDisplay:false
        }
    }
    //hàm cho phép thay đổi giá trị input message
    inputMessageChangeHandler = (value) => {
        console.log("thay doi gia tri inputmessage: " + value);
        this.setState({
            inputMessage:value
        })
    }

    //hàm cho phép thay đổi giá trị output message
    outputMessageChangeHandler = () => {
        if (this.state.inputMessage) {
            this.setState({
                outputMessage:[...this.state.outputMessage, this.state.inputMessage],
                likeDisplay:true
            })
        }
    }
    render() {
        return (
            <>
                <InputBody inputMessageProps={this.state.inputMessage} inputMessageChangeHandlerProp={this.inputMessageChangeHandler} outputMessageChangeHandlerProp={this.outputMessageChangeHandler}/>
                <OutputBody outputMessageProps={this.state.outputMessage} likeDisplayProps={this.state.likeDisplay} />
            </>
        )
    }
}

export default BodyContent