import { Component } from "react";
import like from '../../../assets/images/like-yellow-icon.png'

class OutputBody extends Component {
    render() {
        console.log(this.props);
        return (
            <>
                <div className='row mt-3'>
                    {this.props.outputMessageProps.map((value, index) => {
                        return <p key={index}>{value}</p>
                    })}
                </div>
                <div className='row mt-3'>
                    <div className='col-12'>
                        {this.props.likeDisplayProps ? <img src={like} alt='like' style={{width:"100px"}}/> : null}                        
                    </div>        
                </div>
            </>
        )
    }
}

export default OutputBody