import 'bootstrap/dist/css/bootstrap.min.css'
import BodyContent from './components/body/bodyContent';
import HeaderContent from './components/header/headerContent';

function App() {
  return (
    <div className='container text-center'>
        <HeaderContent/>
        <BodyContent/>       
    </div>
  );
}

export default App;
